terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.73.0"
    }
  }

  backend "s3" {
    # Указываем бакет, где будет храниться файл состояния
    bucket = "diploma-infra-state"
    key    = "global/s3/prod/terraform.tfstate"
    region = "us-east-1"

    # Указываем таблицу для блокировки файла состояния
    dynamodb_table = "diploma-infra-state-locks"
    encrypt = true
  }
}

provider "aws" {
  region = "us-east-1"
}

module "webserver_cluster" {
  source = "../modules/cluster"

  cluster_name           = var.cluster_name
  env_name               = var.env_name

  userdata_file = data.template_file.user_data.rendered

  instance_type = "t2.micro"
  min_size      = 1
  max_size      = 1
}

# Генерируем переменные для стартового скрипт
data "template_file" "user_data" {
  template = file("./user-data.sh")

  vars = {
    env_name = var.env_name
  }
}

# Создаем запись в существующей зоне
resource "aws_route53_record" "www" {
  zone_id = var.zone_id
  name    = "www.awameg.click"
  type    = var.route53_record_type
  ttl     = "300"
  records = [module.webserver_cluster.alb_dns_name]
}