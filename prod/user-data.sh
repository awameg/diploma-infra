#! /bin/bash
docker run -d -p 8080:8080 awameg/awameg-repo:prod
data=`date +%Y-%m-%d_%H-%M-%S`
sed -i 's/varlogs/${env_name}-'$data'/g' /home/ubuntu/mon/promtail.yaml
sudo systemctl restart promtail.service