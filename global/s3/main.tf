provider "aws" {
    region = "us-east-1"
}

# Создаем S3 бакет для хранения файл состояния
resource "aws_s3_bucket" "terraform_state" {
    bucket = "diploma-infra-state"

    # Защищаем от удаления
    lifecycle {
      prevent_destroy = true
    }
    
    # Включаем управление версиями
    versioning {
      enabled = true
    }

    # Включаем шифрование на стороне сервера
    server_side_encryption_configuration {
      rule {
          apply_server_side_encryption_by_default {
              sse_algorithm = "AES256"
          }
      }
    }
}

# Создаем таблицу для блокирования состояния
resource "aws_dynamodb_table" "terraform_lock" {
    name         = "diploma-infra-state-locks"
    billing_mode = "PAY_PER_REQUEST"
    hash_key     =  "LockID"

    attribute {
      name = "LockID"
      type = "S"
    }
}