variable "instance_name" {
  description = "The name to use to namespace all the resources in the instance"
  type        = string
  default     = "node-server"
}

variable "resource_name" {
  description = "The name to use for all the instance resources"
  type        = string
  default     = "node"
}

# zone_id для домена awameg.click
variable "zone_id" {
  description = "Target zone_id to add to route53"
  default = "Z10415422QBS19RYLBB1X"
}

variable "route53_record_type" {
    description = "The record type. Valid values are A, AAAA, CAA, CNAME, MX, NAPTR, NS, PTR, SOA, SPF, SRV and TXT"
    default     = "CNAME"
}