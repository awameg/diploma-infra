terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "3.73.0"
    }
  }

  backend "s3" {
    # Указываем бакет, где будет храниться файл состояния
    bucket = "diploma-infra-state"
    key    = "global/s3/mon/terraform.tfstate"
    region = "us-east-1"

    # Указываем таблицу для блокировки файла состояния
    dynamodb_table = "diploma-infra-state-locks"
    encrypt = true
  }
}

provider "aws" {
  region = "us-east-1"
}

module "mon_server" {
  source = "../../modules/instance"

  instance_name           = var.instance_name
  instance_type           = "t2.micro"
  resource_name           = var.resource_name
}

# Создаем запись в существующей зоне
resource "aws_route53_record" "graf" {
  zone_id = var.zone_id
  name    = "grafana.awameg.click"
  type    = var.route53_record_type
  ttl     = "300"
  records = [module.mon_server.instans_dns]
}

resource "aws_route53_record" "mon" {
  zone_id = var.zone_id
  name    = "monitoring.awameg.click"
  type    = var.route53_record_type
  ttl     = "300"
  records = [module.mon_server.instans_dns]
}
