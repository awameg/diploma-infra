#! /bin/bash
cd ./global/s3 && terrafrom init && terraform apply -auto-approve
sleep 5
cd ../node terrafrom init && terraform apply -auto-approve
sleep 5
cd ../mon terrafrom init && terraform apply -auto-approve
sleep 5
cd ../../stage && terrafrom init && terraform apply -auto-approve
sleep 5
cd ../prod && terrafrom init && terraform apply -auto-approve
sleep 2
echo "stage_env http://infra.awameg.click"
echo "prod_env http://www.awameg.click"