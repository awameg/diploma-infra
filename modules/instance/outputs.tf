output "instans_dns" {
  value       = aws_instance.node-server.public_dns
  description = "The domain name of the instance"
}

# output "asg_name" {
#   value       = aws_autoscaling_group.server.name
#   description = "The name of the Auto Scaling Group"
# }

# output "alb_security_group_id" {
#   value       = aws_security_group.alb.id
#   description = "The ID of the Security Group attached to the load balancer"
# }
