# Описываем инстанс
resource "aws_instance" "node-server" {
  ami = var.ami
  instance_type = var.instance_type
  vpc_security_group_ids = [aws_security_group.node-server.id]
  key_name = aws_key_pair.my_key_pair.key_name

  tags = {
    Name = var.instance_name
  }

  provisioner "remote-exec" {
    inline = ["echo 'connection successfully'"]
    connection {
      host = "${self.public_ip}"
      type = "ssh"
      user = "ubuntu"
      private_key = "${file(var.private_key)}"
    }
  }

  provisioner "local-exec" {
    command = "ansible-playbook -u ubuntu -i '${self.public_ip},' --private-key '${var.private_key}' main.yaml"
  }

  lifecycle {
    create_before_destroy = true
  }
}

# Добавляем ключ пару
resource "aws_key_pair" "my_key_pair" {
  key_name   = "${var.resource_name} gitlab"
  public_key = file("/home/awam/gitlab/gitlab.pub")
}

# Добавляем группу безопасности
resource "aws_security_group" "node-server" {
  name        = "${var.resource_name} Servers Security Group"
  description = "Security group Node-server"

  dynamic "ingress" {
    for_each = var.allowed_ports
    content {
      from_port   = ingress.value
      to_port     = ingress.value
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name  = "Node-server SecurityGroup"
  }
}

