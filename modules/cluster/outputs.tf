output "alb_dns_name" {
  value       = aws_lb.server.dns_name
  description = "The domain name of the load balancer"
}

output "asg_name" {
  value = aws_autoscaling_group.server.name
  description = "The name of the Auto Scaling Group"
}
