# Описываем инстанс
resource "aws_launch_configuration" "server" {
  image_id        = var.ami
  instance_type   = var.instance_type
  security_groups = [aws_security_group.instance.id]
  key_name = aws_key_pair.my_key_pair.key_name

  # user_data = file("${path.module}/user-data.sh")
  user_data = var.userdata_file

  lifecycle {
    create_before_destroy = true
  }
}

# Добавляем ключ пару
resource "aws_key_pair" "my_key_pair" {
  key_name   = "${var.env_name} gitlab"
  public_key = file("/home/awam/gitlab/gitlab.pub")
}

# Добавляем группу отказоустойчивости
resource "aws_autoscaling_group" "server" {
  launch_configuration = aws_launch_configuration.server.name
  vpc_zone_identifier  = data.aws_subnet_ids.default.ids
  target_group_arns    = [aws_lb_target_group.asg.arn]
  health_check_type    = "ELB"

  min_size = var.min_size
  max_size = var.max_size

  tag {
    key                 = "Name"
    value               = var.cluster_name
    propagate_at_launch = true
  }
}

# Группа безопасти для инстансов
resource "aws_security_group" "instance" {
  name = "${var.cluster_name}-instance"
}

# Добавляем правила для входящих стоединений
resource "aws_security_group_rule" "allow_inbound" {
  type              = "ingress"
  security_group_id = aws_security_group.instance.id

  count       = "${length(var.allowed_ports)}"
  from_port   = "${element(var.allowed_ports, count.index)}"
  to_port     = "${element(var.allowed_ports, count.index)}"
  protocol    = local.tcp_protocol
  cidr_blocks = local.all_ips
}

# Добавляем правила для исходящих соединений
resource "aws_security_group_rule" "allow_instance_all_outbound" {
  type              = "egress"
  security_group_id = aws_security_group.instance.id

  from_port   = local.any_port
  to_port     = local.any_port
  protocol    = local.any_protocol
  cidr_blocks = local.all_ips
}

# Создаем балансировщик
resource "aws_lb" "server" {
  name               = var.cluster_name
  load_balancer_type = "application"
  subnets            = data.aws_subnet_ids.default.ids
  security_groups    = [aws_security_group.alb.id]
}

# Создаем прослушиватель
resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.server.arn

  port              = local.http_port

  protocol          = "HTTP"

  # По умолчанию возвращает страницу с кодом 404
  default_action {
    type = "fixed-response"

    fixed_response {
      content_type = "text/plain"
      message_body = "404: page not found"
      status_code  = 404
    }
  }
}

# Перенаправляем трафик к группе отказоустойчивости
resource "aws_lb_target_group" "asg" {
  name     = var.cluster_name
  port     = var.allowed_ports[1]
  protocol = "HTTP"
  vpc_id   = data.aws_vpc.default.id

  health_check {
    # path                = "/"
    # protocol            = "HTTP"
    # matcher             = "200"
    interval            = 70 #15
    timeout             = 60 #3
    healthy_threshold   = 2
    unhealthy_threshold = 10 #2
  }
}

# Правило для прослушивателя
resource "aws_lb_listener_rule" "asg" {
  listener_arn = aws_lb_listener.http.arn
  priority     = 100

  condition {
    path_pattern {
      values = ["*"]
    }
  }

  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.asg.arn
  }
}

# Группа безопасноти для балансировщика
resource "aws_security_group" "alb" {
  name = "${var.cluster_name}-alb"
}

# Правила для входящих соединений
resource "aws_security_group_rule" "allow_http_inbound" {
  type              = "ingress"
  security_group_id = aws_security_group.alb.id

  from_port   = local.http_port
  to_port     = local.http_port
  protocol    = local.tcp_protocol
  cidr_blocks = local.all_ips
}

# Правила для исходящих соединений
resource "aws_security_group_rule" "allow_all_outbound" {
  type              = "egress"
  security_group_id = aws_security_group.alb.id

  from_port   = local.any_port
  to_port     = local.any_port
  protocol    = local.any_protocol
  cidr_blocks = local.all_ips
}

locals {
  http_port    = 80
  any_port     = 0
  any_protocol = "-1"
  tcp_protocol = "tcp"
  all_ips      = ["0.0.0.0/0"]
}

# Получаем данные о подсетях
data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "default" {
  vpc_id = data.aws_vpc.default.id
}
