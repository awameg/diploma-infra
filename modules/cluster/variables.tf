variable "cluster_name" {
  description = "The name to use for all the cluster resources"
  type        = string
}

variable "env_name" {
  description = "The name to use for all the cluster resources"
  type        = string
}

variable "ami" {
  description = "The AMI to run in the cluster"
  type        = string
  default     = "ami-0ac136a5e60f8b26e"
}

variable "instance_type" {
  description = "The type of EC2 Instances to run (e.g. t2.micro)"
  type        = string
}

variable "min_size" {
  description = "The minimum number of EC2 Instances in the ASG"
  type        = number
}

variable "max_size" {
  description = "The maximum number of EC2 Instances in the ASG"
  type        = number
}

variable "allowed_ports" {
    description = "Allowed ports from/to host"
    type        = list
    default     = ["22", "8080"]
}

# zone_id для домена awameg.click
variable "zone_id" {
  description = "Target zone_id to add to route53"
  default = "Z10415422QBS19RYLBB1X"
}

variable "route53_record_type" {
    description = "The record type. Valid values are A, AAAA, CAA, CNAME, MX, NAPTR, NS, PTR, SOA, SPF, SRV and TXT"
    default     = "CNAME"
}

variable "privat_key" {
  default = "~/gitlab/gitlab"
}

variable "userdata_file" {
  description = "The userdata file for provision"
  type        = string
}