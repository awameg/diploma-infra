## diploma-infra 
## Подготовка к развертыванию
        Для развертывания инфраструктуры и запуска приложения, Вам необходимо иметь акаут в Amazon Web Services, также у Вас должен быть настроен Terraform.
        AWS
            * Зарегистрируйте аккаунт в AWS
                Ссылка: https://aws.amazon.com/
            * Установите aws-cli
                Документация: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html
            * Сконфигурируйте aws
                Документация: https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html
        Terraform
            * Установите terraform
                Ссылка: https://learn.hashicorp.com/tutorials/terraform/install-cli
        Ansible
            * Установите ansible
                Ссылка: https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html
        
## Склонируйте репозиторий с инфраструктурой
    git clone https://gitlab.com/awameg/diploma-infra.git

## Склонируйте репозиторий с приложением
    git clone https://gitlab.com/awameg/diploma-service.git
    
    # Структура репозитория с инфраструктурой(diploma-infra):
        * global - глобальная конфигурация для всех сред.
            * s3 разворачивается S3 backet для хранения файлов состояния инфраструктуры
            * node gitlab agent
            * mon сервер мониторинга
        * modules/cluster - основной модуль, для развертывания тестовой инфраструктуры
        * modules/instance - модуль, для развертывания одинчного инстанса
        * stage - корневой модуль тестовой инфраструктуры
            - user-data.sh - скрипт выполняемый при запуске участника ASG
        * prod - корневой модуль промышленной инфраструктуры
            - user-data.sh - скрипт выполняемый при запуске участника ASG
        * LADR запись архитектурного решения
    
## Подготовка к развертыванию сред
    # Перед развертыванием Вам необходимо в репозитории с приложением:
        1. Перейти в Settings / CICD / Runners
        2. Отключить Shared runners
        3. Скопировать registration token для собственного runner
        4. Перейти в папку ./global/node
        5. Создать файл secret
            touch secret
        6. Файл secret должен содержать:
            gitlab_token: ранее_скопированный_токен
        7. Перейти в Settings / CICD / Variables
        9. Создать переменную docker_user - ваше имя пользователя, docker_pass - ваш пароль на https://hub.docker.com/

Теперь вы готовы к развертыванию инфраструктуры

## Развертывание инфраструктуры
    # Автоматическое развертывание:
        После клонирования репозитория запустите лежащий в корне файл start.sh после чего:
        * будет развернута инфраструктура
        * приложение будет доступно по адресу http://infra.awameg.click
    
    # Ручное развертывание:
        После клонирования репозитория выполните следующие действия:
        1. для развертывание S3 bucket перейдите в папку global/s3
                cd ./global/s3
        2. выполните инициализацию
                terrafrom init
        3. разверните инфраструктуру
                terraform apply -auto-approve
        
        * для развертывания gitlab agent-server перейдите в папку global/node
                cd ../node
            1. выполните инициализацию
                terrafrom init
            2. разверните инфраструктуру
                terraform apply -auto-approve
            3. в процессе развертывания будет выполнен ansible-playbook с установкой необходимых компонентов
                * gitlab-runner
        
    # После развертывания gitlab agent-server:
    1. в репозитории с приложением https://gitlab.com/awameg/diploma-service.git автоматически будет создан runner с вашим токеном
        * если необходимо создать новый ранер с другими параметрами, установите нужные параметры в плейбуке runner_reg.yaml и запустите его
    2. будет запущен pipeline со следующими задачами:
        * автоматическое тестированием приложения
        * автоматическаясборка имеджа для тестового окружения
        * сборка имеджа приложения для промышленного окружения запускается вручную
        * доставка имеджа приложения для тестового окружения в репозиторий https://hub.docker.com/r/awameg/awameg-repo:skillbox осуществляется в ручную
        * доставка имеджа приложения для промышленного окружения в репозиторий https://hub.docker.com/r/awameg/awameg-repo:prod осуществляется в ручную
    3. для развертывания приложения необходимо запустить job - deploy_app, после чего имедж будет доставлен в docker hub репозиторий
    4. чтобы изменить docker hub репозиторий на ваш, в .gitlab-ci.yml измените переменную docker_repo

    # После окончания развертывание сервер доступен по адресу http://agent.awameg.click

            1. для развертывания сервера мониторинга перейдите в папку global/mon
                cd ../mon
            2. выполните инициализацию
                terrafrom init
            3. разверните инфраструктуру
                terraform apply -auto-approve
            4. в процессе развертывания будет выполнен ansible-playbook с установкой необходимых компонентов
                * prometheus
                * alertmanager
                * grafana
        
    # После окончания развертывание:
            * сервер, c запущенным Prometheus, доступен по адресу monitoring.awameg.click:9090
            * сервер, c запущенной Grafana, доступен по адресу grafana.awameg.click

    # Развертывание тестового окружения
    # В процессе развертывания кластера, целевые сервера автоматически забирают образ приложения из репозитория по умолчанию: https://hub.docker.com/r/awameg/awameg-repo:skillbox
        * чтобы изменить его на свой отредактируейте файл ./user-data.sh
    
            1. для развертывание кластера перейдите в папку stage
                cd ../stage
            2. выполните инициализацию
                terrafrom init
            3. разверните инфраструктуру
                terraform apply -auto-approve

    # После окончания развертывание приложение будет доступно по адресу http://infra.awameg.click

    # Развертывание промышленного окружения
    # В процессе развертывания кластера, целевые сервера автоматически забирают образ приложения из репозитория по умолчанию: https://hub.docker.com/r/awameg/awameg-repo:prod
    * чтобы изменить его на свой отредактируейте файл ./user-data.sh

            1. для развертывание кластера перейдите в папку prod
                cd ../prod
            2. выполните инициализацию
                terrafrom init
            3. разверните инфраструктуру
                terraform apply -auto-approve

    # После окончания развертывание приложение будет доступно по адресу http://www.awameg.click
